I set up Git on a server hosted by Bitbucket
Tutorial: https://www.youtube.com/watch?v=ov3_CkObQm8

Aufgabenstellung:
Zur Erreichung eines optimalen Ertrags soll ein Solarpanel möglichst optimal zur
Sonne ausgerichtet werden. Die Basiskomponenten einer solchen Anlage existieren
bereits. Das Solarpanel kann mit Hilfe von Elektromotoren (Lego-Mindstorms Motoren)
ausgerichtet werden. Sensoren (Lego-Mindstroms Sensoren) zur Erfassung des
Sonnenstands und der geografischen Ausrichtung, sowie zur Erfassung der
gewonnenen und in einem Akku gespeicherten Energie sind ebenfalls vorhanden.
Basierend auf dem Raspberry Pi Kleinstrechner soll ein Steuerrechner für die
bestehende Anlage konzipiert werden. Dieser ist neben der Ausrichtung des
Solarpanels auch für die Erfassung und Veröffentlichung der Ertragsdaten zuständig.
Die Veröffentlichung soll in Form einer zyklisch aktualisierten Web-Seite erfolgen.